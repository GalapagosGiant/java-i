
package com.mykhaylov.interfaces.taskA4;

interface TestInterface2 {
    
    int intVal = 2;
    double doubleVal = 2.2;
    
    static void showInt()
    {
       System.out.println("TestInterface2: Show int value '" + intVal + "'.");
    }
    
    static void showDouble()
    {
       System.out.println("TestInterface2: Show double value '" + intVal + "'.");
    }
}
