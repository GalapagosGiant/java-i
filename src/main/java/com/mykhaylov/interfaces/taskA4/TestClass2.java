
package com.mykhaylov.interfaces.taskA4;

public class TestClass2 {
    
    public TestClass2()
    {
        System.out.println("TestClass2: TestClass2.");
    }
    
    public void showValueInt()
    {
        TestInterface2.showInt();
    }
    
    public void showValueDouble()
    {
        System.out.println("TestClass2: Show double value '" + TestInterface2.doubleVal + "'.");
    }
}
