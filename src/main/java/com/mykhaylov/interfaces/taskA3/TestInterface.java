
package com.mykhaylov.interfaces.taskA3;

interface TestInterface {
    
    int intVal = 1;
    double doubleVal = 1.1;
    
    void showValueInt();
    void showValueDouble();
}
