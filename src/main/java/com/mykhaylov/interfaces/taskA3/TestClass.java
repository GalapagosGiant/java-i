
package com.mykhaylov.interfaces.taskA3;

public class TestClass implements TestInterface {
    
    public TestClass()
    {
        System.out.println("TestClass: TestClass implements TestInterface.");
    }
    
    @Override
    public void showValueInt()
    {
        System.out.println("TestClass: Int value '" + intVal + "'.");
    }
    
    @Override
    public void showValueDouble()
    {
        System.out.println("TestClass: Double value '" + doubleVal + "'.");
    }
}
