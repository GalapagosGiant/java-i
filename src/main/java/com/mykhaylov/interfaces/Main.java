
package com.mykhaylov.interfaces;

import com.mykhaylov.interfaces.taskA1.Figura;
import com.mykhaylov.interfaces.taskA3.TestClass;
import com.mykhaylov.interfaces.taskA4.TestClass2;
import com.mykhaylov.interfaces.taskB2.MojaKlasa;
import com.mykhaylov.interfaces.taskB3.Radio;
import com.mykhaylov.interfaces.taskB3.Telewizor;

public class Main {
    public static void main (String[] args){
        
        //System.out.println("");

        // Task A1.

        Figura figura = new Figura();
        
        figura.rysuj2D();
        figura.rysuj3D();
        
        // Task A3.

        TestClass testClass = new TestClass();
        
        testClass.showValueInt();
        testClass.showValueDouble();
        
        // Task A4.
        
        TestClass2 testClass2 = new TestClass2();
        
        testClass2.showValueInt();
        testClass2.showValueDouble();
        
        // Task B2.
        
        MojaKlasa mojaKlasa = new MojaKlasa();
        
        mojaKlasa.f(3);
        mojaKlasa.f(3.3);
        
        // Task B3.
        
        Radio radio = new Radio();
        
        radio.graj();
        
        Telewizor telewizor = new Telewizor();
        
        telewizor.graj();
        telewizor.wyswietl();
    }
}
