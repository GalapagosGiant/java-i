
package com.mykhaylov.interfaces.taskB3;

public class Radio implements WydajeDzwiek {
    
    public Radio()
    {
        System.out.println("Radio: Radio implements WydajeDzwiek.");
    }
    
    @Override
    public void graj()
    {
        //instrukcje metody graj
        System.out.println("Radio: Graj.");
    }
}
