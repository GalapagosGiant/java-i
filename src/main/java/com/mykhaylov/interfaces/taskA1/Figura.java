
package com.mykhaylov.interfaces.taskA1;

public class Figura implements Rysowanie {
    
    public Figura()
    {
        System.out.println("Figura: Figura implements Rysowanie.");
    }
    
    @Override
    public void rysuj2D()
    {
        System.out.println("Figura: Rysuj 2D.");
    }
    
    @Override
    public void rysuj3D()
    {
        System.out.println("Figura: Rysuj 3D.");
    }
}
