
package com.mykhaylov.interfaces.taskB2;

public class MojaKlasa implements PierwszyInterfejs, DrugiInterfejs {
    
    public MojaKlasa()
    {
        System.out.println("MojaKlasa: MojaKlasa implements PierwszyInterfejs, DrugiInterfejs.");
    }

    @Override
    public void f(int val)
    {
        System.out.println("MojaKlasa: Print int val " + val + "'.");
    }

    @Override
    public void f(double val)
    {
        System.out.println("MojaKlasa: Print double val " + val + "'.");
    }
}
